import React from 'react'

const About=()=>{
    return(
        <>
        <section id="action">
            <div class="right">
                <div class="container-content line">
                    <h1 class="h1-about">Data Peserta Sanbercode Bootcamp Reactjs</h1>
                  <div class="article-list">
                      <div class="list-s">
                        <ol>
                            <li><h3>Nama:</h3> <p>Muhammad Wava Ferdiansyah Putra</p></li>
                            <li><h3>Email:</h3> <p>mwavafp31sss@gmail.com</p></li>
                            <li><h3>Sistem Operasi yang digunakan:</h3> <p>Windows 10</p></li>
                            <li><h3>Akun Gitlab:</h3> <p>@mwava   dengan nama mwava fp</p></li>
                            <li><h3>Akun Telegram:</h3> <p>@mwava</p></li>
                        </ol>
                        <div class="back">
                            <button type="button"><a href="../index.html">Kembali ke index</a> </button>
                        </div>
                      </div>
                  </div>
                </div>
           
            </div>
        </section>

    <footer>
      <h5>copyright &copy; 2020 by Sanbercode</h5>
    </footer>

        </>
    )
}
export default About