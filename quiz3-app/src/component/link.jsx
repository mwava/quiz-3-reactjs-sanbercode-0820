import React from 'react'
import {Link} from 'react-router-dom'
import logo from '../asset/logo.png'
import './font.css'

const LinkNav=()=>{
    return(
        <>
            <div class="navigasi">
            <img id="logo" src={logo} width="200"/>
               <nav>
                 <ul>
                     <li><Link to="/">Home</Link></li>
                     <li><Link to="/about">About</Link> </li>
                     <li><Link to="/login">Login</Link> </li>
                 </ul>
               </nav>
                 </div>
        </>
    )
}

export default LinkNav