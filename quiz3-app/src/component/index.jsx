import React,{useEffect,useContext} from 'react'
import {FilmContext} from './ContextFilm'
import axios from 'axios'
import './font.css'


const Index=()=>{
  const [daftarfilm,setDaftarFilm] =useContext(FilmContext);

    useEffect(()=>{
        if(daftarfilm === null){
            axios.get('http://backendexample.sanbercloud.com/api/movies')
            .then(res=>{
                setDaftarFilm(res.data)
                console.log(res.data)
            })
        }
    },[daftarfilm]);
    return(
        <>
        
        <section id="action">
        <div class="container-content">
            <h1>Daftar Film Terbaik</h1>
            {
              daftarfilm !== null &&daftarfilm.map((z)=>{
                return(      
                  <div id="article-list">              
                  <div class="list">
                <h3 className="kiri">{z.title}</h3><br></br>
                <img className="pictFilm" src={z.image_url} ></img>
                <div className="infoFilm">
                <h3>Rating: {z.rating}</h3><br></br>
                <h3>Durasi: {z.duration}</h3><br></br>
                <h3>Genre: {z.genre}</h3><br></br>
                <h3>Tahun: {z.year}</h3><br></br>
                </div>
                <p className="deskripsi">
                  Description: {z.description}
                </p>
              </div>
              </div>

                )
              })
            }
            
            
        </div>
    </section>
    <footer>
      <h5>copyright &copy; 2020 by Sanbercode</h5>
    </footer>
       
   


        </>
    )
}
export default Index