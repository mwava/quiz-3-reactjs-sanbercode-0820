import React from 'react'
import {BrowserRouter as Router,Route,Switch} from 'react-router-dom'
import LinkNav from './link'
import About from './page/about'
import Index from './index'
import Login from './page/login'

const Quiz3=()=>{
    return(
        <>
            <Router>
                <LinkNav/>
                <Switch>
                    <Route exact path="/"><Index/></Route>
                    <Route path="/home"><Index/></Route>
                    <Route path="/about"><About/></Route>
                    <Route path="/login"><Login/></Route>
                </Switch>
            </Router>
        </>
    )
}
export default Quiz3