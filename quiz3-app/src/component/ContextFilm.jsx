import React, { useState,createContext } from 'react'

export const FilmContext =createContext();
 const FilmProvide = props=>{
    const [daftarfilm,setDaftarFilm]=useState(null)
    const [input,setInput]=useState({
        id:null,
        title:"",
        description:"" ,
        year:"",
    duration: "",
    genre: "",
    rating: "",
    review: "",
    image_url: ""})

    return(
        <FilmContext.Provider value={[daftarfilm,setDaftarFilm,input,setInput]}>
            {props.children}
        </FilmContext.Provider>
    )
}
export default FilmProvide